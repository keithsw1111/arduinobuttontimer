//http://www.whatimade.today/programming-an-8-digit-7-segment-display-the-easy-way-using-a-max7219/

#include "WS2811.h"

//#define UNO

// Change these if your relay module turns on when data is low or high
#define RELAY_ON 0
#define RELAY_OFF 1

// define these if you want to use a 3 state button to control behaviour
// when these pins are pulled low program goes into the defined state
//#define THREESTATE_ALWAYSON_PIN 14
//#define THREESTATE_TIMER_PIN 15

// These control the pixels
#define HIGHLIGHTS 2
#define HIGHLIGHTSIZE 5
#define HIGHLIGHTCOLOUR 0xFFFFFF
#define LEDS 24
#define LEDSPERCOLOUR 4
#define COLOUR1 0xFF0000
#define COLOUR2 0x00FF00
#define MSBETWEENLEDMOVES 50

// This controls the button the user presses
#define BUTTONLED_PIN 3 
#define BUTTON_PIN 4 
#define BUTTONLEDONMS 500
#define BUTTONLEDOFFMS 200

// This defines the pins used to talk to the led module
#ifdef UNO
#define DISPLAY_SO_PIN 11
#define DISPLAY_CLK_PIN 13
#else
#define DISPLAY_SO_PIN 51
#define DISPLAY_CLK_PIN 52
#define DISPLAY_CS_PIN 53
#endif

// pull one or more of these low to set the time when the user presses the button
#define MIN5_PIN 7
#define MIN10_PIN 8
#define MIN15_PIN 9

// this is the pin the relay is on
#define RELAY_PIN 5

// define this if using the TM1637 7 segment module
//#define USE_TM1637

RGB_t leds[LEDS];
unsigned long setTime = 0;
unsigned long timeOn = 0;

#ifdef UNO
  #define LEDS_PIN 6
  DEFINE_WS2811_FN(MYWS2811, PORTD, 6)
#else
  // need some smarts to set this - 30
  #define LEDS_PIN 30
  DEFINE_WS2811_FN(MYWS2811, PORTC, 7)
#endif

#ifdef USE_TM1637
  #include "TM1637Display.h"
  TM1637Display* tm1637;
#else
  // enumerate the MAX7219 registers
  // See MAX7219 Datasheet, Table 2, page 7
  enum {  MAX7219_REG_DECODE    = 0x09,  
          MAX7219_REG_INTENSITY = 0x0A,
          MAX7219_REG_SCANLIMIT = 0x0B,
          MAX7219_REG_SHUTDOWN  = 0x0C,
          MAX7219_REG_DISPTEST  = 0x0F };

  // enumerate the SHUTDOWN modes
  // See MAX7219 Datasheet, Table 3, page 7
  enum  { OFF = 0,  
          ON  = 1 };

  const byte DP = 0b10000000;  
#endif

void TurnOffLeds()
{
    for (int i = 0; i < LEDS; i++)
    {
        leds[i].r = 0;       
        leds[i].g = 0;
        leds[i].b = 0;
    }
    MYWS2811(leds, LEDS);
}

void SendLeds(long currenttime)
{
	static long nextmove = -1;
	static int highlightpos = 0;

	if (nextmove == -1 || currenttime > nextmove)
	{
		for (int i = 0; i < LEDS; i++)
		{
			if ((i / LEDSPERCOLOUR)%2 == 0)
			{
				leds[i].r = (COLOUR1 & 0xFF0000) >> 16;       
        leds[i].g = (COLOUR1 & 0xFF00) >> 8;
        leds[i].b = COLOUR1 & 0xFF;
			}
			else
			{
        leds[i].r = (COLOUR2 & 0xFF0000) >> 16;
        leds[i].g = (COLOUR2 & 0xFF00) >> 8;
        leds[i].b = COLOUR2 & 0xFF;
			}
		}

    int ledsper = LEDS/HIGHLIGHTS;
    float brightnessper = 1.0 / HIGHLIGHTSIZE;
    int pos = highlightpos;
    float brightness = 1.0;
    for (int j = HIGHLIGHTSIZE - 1 ; j >= 0; j--)
    {
      int r = (HIGHLIGHTCOLOUR & 0xFF000) >> 16;
      int g = (HIGHLIGHTCOLOUR & 0xFF00) >> 8;
      int b = (HIGHLIGHTCOLOUR & 0xFF);
      r = (int)((float)r * brightness);
      g = (int)((float)g * brightness);
      b = (int)((float)b * brightness);

      int k = 0;
      for (int i = 0; i < HIGHLIGHTS; i++)
      {
        if (pos + j + k >= 0 && pos + j + k < LEDS)
        {
          leds[pos+j + k].r = r;
          leds[pos+j + k].g = g;
          leds[pos+j + k].b = b;
        }
        else if (pos + j + k - LEDS >= 0 && pos + j + k - LEDS < LEDS)
        {
          leds[pos+j + k - LEDS].r = r;
          leds[pos+j + k- LEDS].g = g;
          leds[pos+j + k-LEDS].b = b;
        }
        
        k += ledsper;
        if (pos + j + k >= LEDS)
        {
          k -= LEDS;
        }
      }
      
      brightness /= 2;        
    }

    highlightpos++;
    if (highlightpos > LEDS / HIGHLIGHTS + HIGHLIGHTSIZE) highlightpos -= LEDS / HIGHLIGHTS;

		MYWS2811(leds, LEDS);
		
		nextmove = currenttime + MSBETWEENLEDMOVES;
	}
}

void SendButtonLed(long currenttime)
{
	static long nextmove = -1;
	static bool lightstate = false;
	
	if (nextmove == -1 || currenttime > nextmove)
	{
		lightstate = !lightstate;
	
		if (lightstate)
		{
			digitalWrite(BUTTONLED_PIN, 1);
			nextmove = currenttime + BUTTONLEDONMS;
		}
		else
		{
			digitalWrite(BUTTONLED_PIN, 0);
			nextmove = currenttime + BUTTONLEDOFFMS;
		}		
	}
}

#ifdef USE_TM1637
void SendTime(unsigned long time)
{
  static long lasttime = -1;
  
  if (lasttime == (unsigned long)-1 || (time + 10) / 1000 != lasttime)
  {  
    lasttime = time / 1000;

    long disp = (time + 10) / 1000;

    int min10 = disp / 600;
    disp -= (min10 * 600);
    int min1 = disp / 60;
    disp -= min1 * 60;
    int sec10 = disp / 10;
    disp -= sec10 * 10;
    int sec1 = disp;

    // Set the time
    uint8_t digits[4];
    digits[0] = tm1637->encodeDigit(min10);
    digits[1] = 0x80 | tm1637->encodeDigit(min1);
    digits[2] = 0x80 | tm1637->encodeDigit(sec10);
    digits[3] = tm1637->encodeDigit(sec1);
    tm1637->setSegments(digits);
  }
}

#else

void set_register(byte reg, byte value)  
{
    digitalWrite(DISPLAY_CS_PIN, LOW);
    shiftOut(DISPLAY_SO_PIN, DISPLAY_CLK_PIN, MSBFIRST, reg);
    shiftOut(DISPLAY_SO_PIN, DISPLAY_CLK_PIN, MSBFIRST, value);
    digitalWrite(DISPLAY_CS_PIN, HIGH);
}

void SendTime(unsigned long time)
{
	static long lasttime = -1;
  
	if (lasttime == (unsigned long)-1 || (time + 10) / 1000 != lasttime)
	{  
		lasttime = time / 1000;

    long disp = (time + 10) / 1000;

		int min10 = disp / 600;
		disp -= (min10 * 600);
		int min1 = disp / 60;
		disp -= min1 * 60;
		int sec10 = disp / 10;
		disp -= sec10 * 10;
		int sec1 = disp;

		// Set the time
    set_register(MAX7219_REG_SHUTDOWN, OFF);      // turn off display
    set_register(1, 48+sec1);
    set_register(2, 48+sec10);
    set_register(3, 48+min1 | DP);
    set_register(4, 48+min10);
    set_register(MAX7219_REG_SHUTDOWN, ON);       // Turn on display
	}
}
#endif

void SendRelay(unsigned long time)
{
	static bool state = true;
	
	if (time > 0 && state == false)
	{
    Serial.println("Turning relay on");
		digitalWrite(RELAY_PIN, RELAY_ON);
		state = true;
	}
	else if (time == 0 && state == true)
	{
    Serial.println("Turning relay off");
		digitalWrite(RELAY_PIN, RELAY_OFF);
		state = false;
	}
}

void setup()
{
  Serial.begin(115200);
  Serial.println("Setup.");

  pinMode(DISPLAY_SO_PIN, OUTPUT);
  pinMode(DISPLAY_CLK_PIN, OUTPUT);

  #ifdef THREESTATE_ALWAYSON_PIN
  pinMode(THREESTATE_ALWAYSON_PIN, INPUT);
  digitalWrite(THREESTATE_ALWAYSON_PIN, 1);
  pinMode(THREESTATE_TIMER_PIN, INPUT);
  digitalWrite(THREESTATE_TIMER_PIN, 1);
  #endif

	pinMode(LEDS_PIN, OUTPUT);
	digitalWrite(LEDS_PIN, 0);

	pinMode(BUTTONLED_PIN, OUTPUT);
	digitalWrite(BUTTONLED_PIN, 0);

	pinMode(RELAY_PIN, OUTPUT);
	digitalWrite(RELAY_PIN, RELAY_OFF);

	pinMode(BUTTON_PIN, INPUT);
	digitalWrite(BUTTON_PIN, 1);

	pinMode(MIN5_PIN, INPUT);
	digitalWrite(MIN5_PIN, 1);
	pinMode(MIN10_PIN, INPUT);
	digitalWrite(MIN10_PIN, 1);
	pinMode(MIN15_PIN, INPUT);
	digitalWrite(MIN15_PIN, 1);

  setTime = 0;
	if (digitalRead(MIN5_PIN) == 0)
	{
    setTime += 300000;
    //setTime += 5000;
	}
	if (digitalRead(MIN10_PIN) == 0)
	{
    setTime += 600000;
    //setTime += 10000;
	}
	if (digitalRead(MIN15_PIN) == 0)
	{
		setTime += 300000;
    //setTime += 15000;
	}

  //Serial.print("Total time: ");
  //Serial.print(setTime / 1000);
  //Serial.println(" Seconds");

#ifdef USE_TM1637
  tm1637 = new TM1637Display(DISPLAY_CLK_PIN, DISPLAY_SO_PIN);
  tm1637->setBrightness(7);
#else
  pinMode(DISPLAY_CS_PIN, OUTPUT);
  set_register(MAX7219_REG_SHUTDOWN, OFF);      // turn off display
  set_register(MAX7219_REG_SCANLIMIT, 3);       // limit to 8 digits
  set_register(MAX7219_REG_DECODE, 0b00001111); // decode all digits
#endif
    
  timeOn = 0;
	SendTime(0);
  SendRelay(0);
}

void loop()
{
  #ifdef THREESTATE_ALWAYSON_PIN
  int ao = digitalRead(THREESTATE_ALWAYSON_PIN);
  int t = digitalRead(THREESTATE_TIMER_PIN);
  if (ao == 0)
  {
    Serial.println("always on");
    SendRelay(1000);
    SendTime(600000);
    timeOn = 0;
    TurnOffLeds();
    return;
  }
  else if (t == 0)
  {
    // let it through
  }
  else
  {
    // we are off so do nothing
    Serial.println("always off");
    SendRelay(0);
    SendTime(0);
    TurnOffLeds();
    timeOn = 0;
    return;
  }
  #endif
  
	int button = digitalRead(BUTTON_PIN);

  unsigned long CurrentTime = millis();

	if (button == 0)
	{
		timeOn = CurrentTime;
	}

  unsigned long left = 0;

  if (timeOn != 0)
  {
    if (CurrentTime - timeOn > setTime)
    {
      timeOn = 0;
    }
    else
    {
      left = setTime - (CurrentTime - timeOn);
    }
  }

  //if (left != 0 && left % 1000 == 0)
  //{
  //Serial.print("Time left ");
  //Serial.println(left);
  //}
  
	SendRelay(left);
	SendTime(left);
	SendButtonLed(CurrentTime);
	SendLeds(CurrentTime);
}
